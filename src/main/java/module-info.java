module com.simplon.cdajava.myfirstjavafxproject {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.simplon.cdajava.myfirstjavafxproject to javafx.fxml;
    exports com.simplon.cdajava.myfirstjavafxproject;
}